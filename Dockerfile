# Build jollyroger first and then copy the war to tomcat
FROM openjdk:8-alpine as jollyroger-builder

RUN apk add --no-cache apache-ant git unzip
RUN git clone https://gitlab.com/ucsdlibrary/development/jollyroger.git /tmp/jollyroger
WORKDIR /tmp/jollyroger
RUN ant webapp
RUN mkdir -p /pub/dams
RUN mv /tmp/jollyroger/dist/jollyroger.war /pub/dams/

FROM tomcat:7-jre8-alpine as production

# Environment defaults. Obviously, uh, *development*
ENV MANAGER_USER tomcat
ENV MANAGER_PASS tomcat

# setup config and other required files
COPY tomcat/tomcat-users.xml /usr/local/tomcat/conf/tomcat-users.xml
COPY tomcat/server.xml /usr/local/tomcat/conf/server.xml

# Install jollyroger
COPY --from=jollyroger-builder /pub/dams/jollyroger.war /usr/local/tomcat/webapps/
