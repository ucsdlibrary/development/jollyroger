jolly roger
===========

fetches marc records from III (roger.ucsd.edu) and converts them to marcxml or modsxml.

### Use registered image in Gitlab for test
The CI pineline will build the image with kanito and registered it in Gitlab. Run the following command to start jollyroger:
```
docker run -it --rm -p 8080:8080 registry.gitlab.com/ucsdlibrary/development/jollyroger:stable
```

It's now available at http://localhost:8080/jollyroger. In MAC OSX, may need to use the container's IP address instead.